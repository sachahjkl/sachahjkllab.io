# Site web personnel de Sacha FROMENT

Vous retrouverez dans ce dépot les fichiers que j'utilise pour générer mon site perso.

J'utilise comme outils pour le générer :

- **hugo** pour la gestion des templates et du contenu
- **sass** pour la compilation du css
