---
icon: 🌐
title: Espace numérique de travail
date: 2021-02-02T23:40:23+01:00
img: "/img/projets/ent.png"
project_url: https://gitlab.com/cnam_a1_sys/client_serv_ipc
draft: false
---

Site web de gestionnaire d'emploi du temps, d'élèves & professeurs, cours, devoirs, etc.

Déployé sur une instances VPS linux à l'aide du serveur [NGINX](https://www.nginx.com/), une base de données [PostgreSQL](https://www.postgresql.org/), du langage PHP en tandem avec le framework [Symfony](https://symfony.com/). Les échanges HTTP sont sécurisé grâce à un certificat TLS auto-renouvellé [Let's Encrypt](https://letsencrypt.org/fr/).
