---
icon: ⚙️
title: "Architecture des Machines et des Système "
date: 2021-02-02T23:40:10+01:00
img: "/img/projets/ipc.png"
project_url: https://gitlab.com/cnam_a1_sys/client_serv_ipc
languages:
  - C
draft: false
---

L'objectif de ce projet est de mettre en oeuvre un système de processus clients/serveurs permettant d'offrirs des services aux utilisateurs.

Afin de mettre en place ce système, une des contraintes est d'utiliser les systèmes de communication inter-processus disponibles au sein des systèmes UNIX. Notamment les tubes nommés(ou non), les files de messages ou encore les sémaphores.
