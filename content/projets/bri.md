---
icon: 🖧
title: Plateforme de services dynamiques
date: 2021-02-02T23:40:33+01:00
img: "/img/projets/bri.png"
project_url: https://gitlab.com/sachahjkl/dut_a2_appref_bri
draft: false
---

Projet Java utilisant une architecture client <-> serveur à la [OSGi](https://www.osgi.org/). L'objectif est de fournir un service à des utilisateurs une plateforme leur permettant d'uploader & partager leurs programmes (suivant un patterne défini) avec d'autres utilisateurs.

Ce projet met en place un système d'authentification, d'upload et implémente un certain nombre de services à titre d'exemples.
