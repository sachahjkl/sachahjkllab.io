---
title: "Page d'accueil"
date: 2021-02-02T22:38:42+01:00
draft: false
---

# Sacha Froment

------

## Informations de contact / détails

[à propos de moi](/a-propos)

<!-- Mon [CV](/pdf/cv-public-de-sacha-froment.pdf) -->

📧 [M'envoyer un mail](mailto:sacha@sacha.house?subject=Contact%20Sacha%20FROMENT&body=Bonjour%20M.Froment%2C)

------

 <a class="centered" href="/img/c-omrade.jpg">
        <img
        class="centered__image"
        src="/img/c-omrade.jpg"
        alt="c-omrade"
        style="max-height: 500px"
        />
</a>
