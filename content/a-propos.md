---
title: "à propos"
date: 2021-02-02T23:31:18+01:00
draft: false
---

<a class="centered" href="/img/me.jpg">
    <img class="centered__image" src="/img/me.jpg" alt="sacha froment" style="max-width: 300px">
</a>

Je m'appelle Sacha Froment, j'habite à Paris.

Je suis ingénieur en informatique en apprentissage à [AG2R La Mondiale](https://www.ag2rlamondiale.fr/).

N'hésitez pas à m'envoyer un mail à propos de ce que vous pensez de ce site ou tout autre sujet qui pourrait m'intéresser.

## Détails professionels

J'ai travaillé à [Italic](https://italic.fr/) en tant qu'administrateur système pendant 6 mois.

J'ai également fais un travail d'été pour la mairie de paris à la [DJS](https://www.service-public.pf/djs/).

<!-- Pour plus de détails, voici mon [CV](/pdf/cv-public-de-sacha-froment.pdf). -->
